-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 28, 2020 at 05:45 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gokhshtein-backend`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2020-10-21 23:49:18', '2020-10-21 23:49:18'),
(2, NULL, 1, 'Category 2', 'category-2', '2020-10-21 23:49:18', '2020-10-21 23:49:18');

-- --------------------------------------------------------

--
-- Table structure for table `contact_details`
--

CREATE TABLE `contact_details` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(65) NOT NULL,
  `phone_number` varchar(65) NOT NULL,
  `message` longtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enquiry_type` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_details`
--

INSERT INTO `contact_details` (`id`, `name`, `email`, `phone_number`, `message`, `created_at`, `enquiry_type`) VALUES
(1, 'puniu', 'gdshs@sfs.com', '1234256465', 'TEst', '2020-10-26 09:58:55', '');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(57, 7, 'cat_title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(58, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 3),
(59, 7, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 4),
(60, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(61, 9, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(62, 9, 'podcast_title', 'text', 'Podcast Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(63, 9, 'podcast_image', 'image', 'Podcast Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(64, 9, 'podcast_date', 'date', 'Podcast Date', 0, 1, 1, 1, 1, 1, '{}', 4),
(65, 9, 'podcast_link', 'text', 'Podcast Link', 0, 1, 1, 1, 1, 1, '{}', 5),
(66, 9, 'podcast_category', 'text', 'Podcast Category', 0, 1, 1, 1, 1, 1, '{}', 6),
(67, 9, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 7),
(68, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(69, 9, 'podcast_belongsto_podcast_category_relationship', 'relationship', 'Podcast Categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\PodcastCategory\",\"table\":\"podcast_categories\",\"type\":\"belongsTo\",\"column\":\"podcast_category\",\"key\":\"id\",\"label\":\"cat_title\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(70, 10, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(71, 10, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(72, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(73, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(74, 11, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(75, 11, 'magazine_title', 'text', 'Magazine Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(76, 11, 'magazine_link', 'file', 'Magazine', 0, 1, 1, 1, 1, 1, '{}', 3),
(77, 11, 'magazine_pages', 'text', 'Magazine Pages', 0, 1, 1, 1, 1, 1, '{}', 4),
(78, 11, 'magazine_poster', 'image', 'Magazine Poster', 0, 1, 1, 1, 1, 1, '{}', 5),
(79, 11, 'magazine_description', 'rich_text_box', 'Magazine Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(80, 11, 'magazine_tag', 'text', 'Magazine Tag', 0, 1, 1, 1, 1, 1, '{}', 7),
(81, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 0, 0, 0, 0, '{}', 8),
(82, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(83, 11, 'magazine_belongsto_magazines_category_relationship', 'relationship', 'Magazine Category', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MagazinesCategory\",\"table\":\"magazines_categories\",\"type\":\"belongsTo\",\"column\":\"magazine_category\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(84, 11, 'magazine_category', 'text', 'Magazine Category', 0, 1, 1, 1, 1, 1, '{}', 10),
(85, 12, 'id', 'number', 'Sl No.', 1, 1, 0, 0, 0, 0, '{}', 1),
(86, 12, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(87, 12, 'phone_number', 'text', 'Phone Number', 1, 1, 1, 1, 1, 1, '{}', 3),
(88, 12, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4),
(89, 12, 'magazine_id', 'text', 'Magazine', 1, 1, 1, 1, 1, 1, '{}', 5),
(90, 12, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 1, 0, 1, '{}', 6),
(91, 12, 'magazine_download_belongsto_magazine_relationship', 'relationship', 'Magazine Downloaded', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Magazine\",\"table\":\"magazines\",\"type\":\"belongsTo\",\"column\":\"magazine_id\",\"key\":\"id\",\"label\":\"magazine_title\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(92, 13, 'id', 'number', 'Sl No.', 1, 1, 1, 0, 0, 0, '{}', 1),
(93, 13, 'name', 'text', 'Name', 1, 1, 1, 0, 0, 0, '{}', 2),
(94, 13, 'email', 'text', 'Email', 1, 1, 1, 0, 0, 0, '{}', 3),
(95, 13, 'phone_number', 'text', 'Phone Number', 1, 1, 1, 0, 0, 0, '{}', 5),
(96, 13, 'message', 'text_area', 'Message', 1, 1, 1, 0, 0, 0, '{}', 6),
(97, 13, 'created_at', 'text', 'Created At', 1, 1, 1, 0, 0, 0, '{}', 7),
(98, 13, 'enquiry_type', 'text', 'Enquiry Type', 1, 1, 1, 1, 1, 1, '{}', 4);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-10-21 23:49:09', '2020-10-21 23:49:09'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-10-21 23:49:10', '2020-10-21 23:49:10'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-10-21 23:49:10', '2020-10-21 23:49:10'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2020-10-21 23:49:18', '2020-10-21 23:49:18'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-10-21 23:49:19', '2020-10-21 23:49:19'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2020-10-21 23:49:20', '2020-10-21 23:49:20'),
(7, 'podcast_categories', 'podcast-categories', 'Podcast Category', 'Podcast Categories', 'voyager-categories', 'App\\PodcastCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-22 00:25:04', '2020-10-22 01:22:11'),
(8, 'podcast', 'podcast', 'Podcast', 'Podcasts', 'voyager-youtube-play', 'App\\Podcast', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-22 01:34:24', '2020-10-22 01:34:24'),
(9, 'podcasts', 'podcasts', 'Podcast', 'Podcasts', 'voyager-youtube-play', 'App\\Podcast', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-22 01:36:05', '2020-10-22 01:45:02'),
(10, 'magazines_categories', 'magazines-categories', 'Magazines Category', 'Magazines Categories', 'voyager-treasure-open', 'App\\MagazinesCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-22 02:23:32', '2020-10-22 02:23:32'),
(11, 'magazines', 'magazines', 'Magazine', 'Magazines', 'voyager-documentation', 'App\\Magazine', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-22 03:24:24', '2020-10-27 23:46:36'),
(12, 'magazine_downloads', 'magazine-downloads', 'Magazine Download', 'Magazine Downloads', 'voyager-data', 'App\\MagazineDownload', NULL, 'App\\Http\\Controllers\\Voyager\\MagazineController', NULL, 0, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-22 07:04:39', '2020-10-25 19:42:00'),
(13, 'contact_details', 'contact-details', 'Contact Detail', 'Contact Details', 'voyager-group', 'App\\ContactDetail', NULL, 'App\\Http\\Controllers\\Voyager\\ContactController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-25 22:33:06', '2020-10-28 00:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `magazines`
--

CREATE TABLE `magazines` (
  `id` int(10) UNSIGNED NOT NULL,
  `magazine_title` text COLLATE utf8mb4_unicode_ci,
  `magazine_link` text COLLATE utf8mb4_unicode_ci,
  `magazine_pages` varchar(65) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `magazine_poster` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `magazine_description` longtext COLLATE utf8mb4_unicode_ci,
  `magazine_tag` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `magazine_category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `magazines`
--

INSERT INTO `magazines` (`id`, `magazine_title`, `magazine_link`, `magazine_pages`, `magazine_poster`, `magazine_description`, `magazine_tag`, `created_at`, `updated_at`, `magazine_category`) VALUES
(1, 'Women In Crypto - 2019 – April Issue', '[{\"download_link\":\"magazines\\\\October2020\\\\mQ15OzqvYjdghXi3lgtF.pdf\",\"original_name\":\"VSp.pdf\"}]', '129+', 'magazines\\October2020\\YN8g3ugB1eWcajgKcRvp.jpg', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.1rem; color: #000000; font-family: Montserrat, sans-serif; font-size: 12px;\" data-v-21dc8167=\"\"><span style=\"box-sizing: border-box; font-weight: bolder;\" data-v-21dc8167=\"\">#WomenOfCrypto:</span>&nbsp;Gokhshtein&rsquo;s Top 20</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.1rem; color: #000000; font-family: Montserrat, sans-serif; font-size: 12px;\" data-v-21dc8167=\"\"><span style=\"box-sizing: border-box; font-weight: bolder;\" data-v-21dc8167=\"\">Decentraland:</span>&nbsp;A Virtual World For A Virtual Future</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.1rem; color: #000000; font-family: Montserrat, sans-serif; font-size: 12px;\" data-v-21dc8167=\"\"><span style=\"box-sizing: border-box; font-weight: bolder;\" data-v-21dc8167=\"\">Traditional Markets v Digital Assets:</span>&nbsp;Are They Safer Than Crypto?</p>\r\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.1rem; color: #000000; font-family: Montserrat, sans-serif; font-size: 12px;\" data-v-21dc8167=\"\"><span style=\"box-sizing: border-box; font-weight: bolder;\" data-v-21dc8167=\"\">Investing in Luxury Watches:</span>&nbsp;5 Great Watches Under $3,000</p>', '2019 April Issue', '2020-10-22 03:34:00', '2020-10-23 02:52:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `magazines_categories`
--

CREATE TABLE `magazines_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `magazines_categories`
--

INSERT INTO `magazines_categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Magazines', '2020-10-22 03:16:00', '2020-10-22 03:16:00');

-- --------------------------------------------------------

--
-- Table structure for table `magazine_downloads`
--

CREATE TABLE `magazine_downloads` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `magazine_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `magazine_downloads`
--

INSERT INTO `magazine_downloads` (`id`, `name`, `phone_number`, `email`, `magazine_id`, `created_at`) VALUES
(1, 'test', '12345678', 'sdfsd@asfsdf.com', 2, '2020-10-23 08:57:12'),
(2, 'testf', '43454', 'ssss@asfsdf.com', 5, '2020-10-23 08:58:07'),
(3, 'fdsdgd', '4354656', 'ssss@asfsdf.com', 5, '2020-10-23 09:23:50'),
(4, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:27:23'),
(5, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:29:01'),
(6, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:29:17'),
(7, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:29:45'),
(8, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:30:01'),
(9, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:30:30'),
(10, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:34:03'),
(11, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:34:26'),
(12, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:34:44'),
(13, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:35:21'),
(14, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:35:39'),
(15, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:36:19'),
(16, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:57:36'),
(17, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 09:57:50'),
(18, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:00:17'),
(19, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:01:22'),
(20, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:01:40'),
(21, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:02:30'),
(22, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:05:33'),
(23, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:07:49'),
(24, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:09:56'),
(25, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:12:57'),
(26, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:14:38'),
(27, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:15:35'),
(28, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:16:02'),
(29, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:16:56'),
(30, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:19:27'),
(31, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:21:01'),
(32, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:21:26'),
(33, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:21:55'),
(34, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:23:48'),
(35, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:24:41'),
(36, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:32:45'),
(37, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:34:14'),
(38, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:37:48'),
(39, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:38:51'),
(40, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:48:05'),
(41, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 10:49:25'),
(42, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-23 12:12:02'),
(43, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-26 01:02:48'),
(44, 'fdsdgdd', '4354656346', 'ssss@asfsdf.com', 1, '2020-10-26 03:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-10-21 23:49:11', '2020-10-21 23:49:11');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-10-21 23:49:11', '2020-10-21 23:49:11', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 10, '2020-10-21 23:49:11', '2020-10-27 23:41:59', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 9, '2020-10-21 23:49:11', '2020-10-27 23:42:03', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 8, '2020-10-21 23:49:11', '2020-10-27 23:42:03', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 14, '2020-10-21 23:49:11', '2020-10-27 23:41:59', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-10-21 23:49:11', '2020-10-22 00:26:56', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-10-21 23:49:11', '2020-10-22 00:26:56', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-10-21 23:49:11', '2020-10-22 00:26:56', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-10-21 23:49:11', '2020-10-22 00:26:56', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 15, '2020-10-21 23:49:11', '2020-10-27 23:41:59', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 13, '2020-10-21 23:49:18', '2020-10-27 23:41:59', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 11, '2020-10-21 23:49:19', '2020-10-27 23:41:59', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 12, '2020-10-21 23:49:20', '2020-10-27 23:41:59', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-10-21 23:49:24', '2020-10-22 00:26:56', 'voyager.hooks', NULL),
(15, 1, 'Podcast Categories', '', '_self', 'voyager-categories', '#000000', NULL, 2, '2020-10-22 00:25:04', '2020-10-22 00:27:27', 'voyager.podcast-categories.index', 'null'),
(17, 1, 'Podcasts', '', '_self', 'voyager-youtube-play', NULL, NULL, 3, '2020-10-22 01:36:06', '2020-10-22 01:37:15', 'voyager.podcasts.index', NULL),
(18, 1, 'Magazines Categories', '', '_self', 'voyager-treasure-open', NULL, NULL, 4, '2020-10-22 02:23:32', '2020-10-22 02:24:00', 'voyager.magazines-categories.index', NULL),
(19, 1, 'Magazines', '', '_self', 'voyager-documentation', NULL, NULL, 5, '2020-10-22 03:24:24', '2020-10-22 03:24:50', 'voyager.magazines.index', NULL),
(20, 1, 'Magazine Downloads', '', '_self', 'voyager-data', '#000000', NULL, 6, '2020-10-22 07:04:40', '2020-10-22 07:10:58', 'voyager.magazine-downloads.index', 'null'),
(21, 1, 'Contact Details', '', '_self', 'voyager-group', '#000000', NULL, 7, '2020-10-25 22:33:07', '2020-10-27 23:42:03', 'voyager.contact-details.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2016_01_01_000000_create_pages_table', 2),
(25, '2016_01_01_000000_create_posts_table', 2),
(26, '2016_02_15_204651_create_categories_table', 2),
(27, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-10-21 23:49:21', '2020-10-21 23:49:21');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-10-21 23:49:11', '2020-10-21 23:49:11'),
(2, 'browse_bread', NULL, '2020-10-21 23:49:11', '2020-10-21 23:49:11'),
(3, 'browse_database', NULL, '2020-10-21 23:49:11', '2020-10-21 23:49:11'),
(4, 'browse_media', NULL, '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(5, 'browse_compass', NULL, '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(6, 'browse_menus', 'menus', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(7, 'read_menus', 'menus', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(8, 'edit_menus', 'menus', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(9, 'add_menus', 'menus', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(10, 'delete_menus', 'menus', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(11, 'browse_roles', 'roles', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(12, 'read_roles', 'roles', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(13, 'edit_roles', 'roles', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(14, 'add_roles', 'roles', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(15, 'delete_roles', 'roles', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(16, 'browse_users', 'users', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(17, 'read_users', 'users', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(18, 'edit_users', 'users', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(19, 'add_users', 'users', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(20, 'delete_users', 'users', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(21, 'browse_settings', 'settings', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(22, 'read_settings', 'settings', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(23, 'edit_settings', 'settings', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(24, 'add_settings', 'settings', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(25, 'delete_settings', 'settings', '2020-10-21 23:49:12', '2020-10-21 23:49:12'),
(26, 'browse_categories', 'categories', '2020-10-21 23:49:18', '2020-10-21 23:49:18'),
(27, 'read_categories', 'categories', '2020-10-21 23:49:18', '2020-10-21 23:49:18'),
(28, 'edit_categories', 'categories', '2020-10-21 23:49:18', '2020-10-21 23:49:18'),
(29, 'add_categories', 'categories', '2020-10-21 23:49:18', '2020-10-21 23:49:18'),
(30, 'delete_categories', 'categories', '2020-10-21 23:49:18', '2020-10-21 23:49:18'),
(31, 'browse_posts', 'posts', '2020-10-21 23:49:19', '2020-10-21 23:49:19'),
(32, 'read_posts', 'posts', '2020-10-21 23:49:19', '2020-10-21 23:49:19'),
(33, 'edit_posts', 'posts', '2020-10-21 23:49:19', '2020-10-21 23:49:19'),
(34, 'add_posts', 'posts', '2020-10-21 23:49:20', '2020-10-21 23:49:20'),
(35, 'delete_posts', 'posts', '2020-10-21 23:49:20', '2020-10-21 23:49:20'),
(36, 'browse_pages', 'pages', '2020-10-21 23:49:20', '2020-10-21 23:49:20'),
(37, 'read_pages', 'pages', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(38, 'edit_pages', 'pages', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(39, 'add_pages', 'pages', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(40, 'delete_pages', 'pages', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(41, 'browse_hooks', NULL, '2020-10-21 23:49:24', '2020-10-21 23:49:24'),
(42, 'browse_podcast_categories', 'podcast_categories', '2020-10-22 00:25:04', '2020-10-22 00:25:04'),
(43, 'read_podcast_categories', 'podcast_categories', '2020-10-22 00:25:04', '2020-10-22 00:25:04'),
(44, 'edit_podcast_categories', 'podcast_categories', '2020-10-22 00:25:04', '2020-10-22 00:25:04'),
(45, 'add_podcast_categories', 'podcast_categories', '2020-10-22 00:25:04', '2020-10-22 00:25:04'),
(46, 'delete_podcast_categories', 'podcast_categories', '2020-10-22 00:25:04', '2020-10-22 00:25:04'),
(47, 'browse_podcast', 'podcast', '2020-10-22 01:34:24', '2020-10-22 01:34:24'),
(48, 'read_podcast', 'podcast', '2020-10-22 01:34:24', '2020-10-22 01:34:24'),
(49, 'edit_podcast', 'podcast', '2020-10-22 01:34:24', '2020-10-22 01:34:24'),
(50, 'add_podcast', 'podcast', '2020-10-22 01:34:24', '2020-10-22 01:34:24'),
(51, 'delete_podcast', 'podcast', '2020-10-22 01:34:24', '2020-10-22 01:34:24'),
(52, 'browse_podcasts', 'podcasts', '2020-10-22 01:36:06', '2020-10-22 01:36:06'),
(53, 'read_podcasts', 'podcasts', '2020-10-22 01:36:06', '2020-10-22 01:36:06'),
(54, 'edit_podcasts', 'podcasts', '2020-10-22 01:36:06', '2020-10-22 01:36:06'),
(55, 'add_podcasts', 'podcasts', '2020-10-22 01:36:06', '2020-10-22 01:36:06'),
(56, 'delete_podcasts', 'podcasts', '2020-10-22 01:36:06', '2020-10-22 01:36:06'),
(57, 'browse_magazines_categories', 'magazines_categories', '2020-10-22 02:23:32', '2020-10-22 02:23:32'),
(58, 'read_magazines_categories', 'magazines_categories', '2020-10-22 02:23:32', '2020-10-22 02:23:32'),
(59, 'edit_magazines_categories', 'magazines_categories', '2020-10-22 02:23:32', '2020-10-22 02:23:32'),
(60, 'add_magazines_categories', 'magazines_categories', '2020-10-22 02:23:32', '2020-10-22 02:23:32'),
(61, 'delete_magazines_categories', 'magazines_categories', '2020-10-22 02:23:32', '2020-10-22 02:23:32'),
(62, 'browse_magazines', 'magazines', '2020-10-22 03:24:24', '2020-10-22 03:24:24'),
(63, 'read_magazines', 'magazines', '2020-10-22 03:24:24', '2020-10-22 03:24:24'),
(64, 'edit_magazines', 'magazines', '2020-10-22 03:24:24', '2020-10-22 03:24:24'),
(65, 'add_magazines', 'magazines', '2020-10-22 03:24:24', '2020-10-22 03:24:24'),
(66, 'delete_magazines', 'magazines', '2020-10-22 03:24:24', '2020-10-22 03:24:24'),
(67, 'browse_magazine_downloads', 'magazine_downloads', '2020-10-22 07:04:40', '2020-10-22 07:04:40'),
(68, 'read_magazine_downloads', 'magazine_downloads', '2020-10-22 07:04:40', '2020-10-22 07:04:40'),
(69, 'edit_magazine_downloads', 'magazine_downloads', '2020-10-22 07:04:40', '2020-10-22 07:04:40'),
(70, 'add_magazine_downloads', 'magazine_downloads', '2020-10-22 07:04:40', '2020-10-22 07:04:40'),
(71, 'delete_magazine_downloads', 'magazine_downloads', '2020-10-22 07:04:40', '2020-10-22 07:04:40'),
(72, 'browse_contact_details', 'contact_details', '2020-10-25 22:33:06', '2020-10-25 22:33:06'),
(73, 'read_contact_details', 'contact_details', '2020-10-25 22:33:06', '2020-10-25 22:33:06'),
(74, 'edit_contact_details', 'contact_details', '2020-10-25 22:33:06', '2020-10-25 22:33:06'),
(75, 'add_contact_details', 'contact_details', '2020-10-25 22:33:06', '2020-10-25 22:33:06'),
(76, 'delete_contact_details', 'contact_details', '2020-10-25 22:33:06', '2020-10-25 22:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(12, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 2),
(15, 3),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(26, 2),
(26, 3),
(27, 1),
(27, 2),
(27, 3),
(28, 1),
(28, 2),
(28, 3),
(29, 1),
(29, 2),
(29, 3),
(30, 1),
(30, 2),
(30, 3),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(33, 1),
(33, 2),
(33, 3),
(34, 1),
(34, 2),
(34, 3),
(35, 1),
(35, 2),
(35, 3),
(36, 1),
(36, 2),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(38, 3),
(39, 1),
(39, 2),
(39, 3),
(40, 1),
(40, 2),
(40, 3),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1);

-- --------------------------------------------------------

--
-- Table structure for table `podcasts`
--

CREATE TABLE `podcasts` (
  `id` int(10) UNSIGNED NOT NULL,
  `podcast_title` longtext COLLATE utf8mb4_unicode_ci,
  `podcast_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `podcast_date` date DEFAULT NULL,
  `podcast_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `podcast_category` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `podcasts`
--

INSERT INTO `podcasts` (`id`, `podcast_title`, `podcast_image`, `podcast_date`, `podcast_link`, `podcast_category`, `created_at`, `updated_at`) VALUES
(1, 'Gokhshtein Unfiltered - October 14 2020', 'podcasts\\October2020\\DGViTnRW9OjuVi7otn84.jpg', '2020-10-15', 'https://www.youtube.com/watch?v=cbFYqwzxGwY&list=PL_UnIDIwT95NureFNedKN9_GA6UOiS2ox', 2, '2020-10-22 01:41:00', '2020-10-22 01:41:46');

-- --------------------------------------------------------

--
-- Table structure for table `podcast_categories`
--

CREATE TABLE `podcast_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_title` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `podcast_categories`
--

INSERT INTO `podcast_categories` (`id`, `cat_title`, `created_at`, `updated_at`) VALUES
(1, 'Blockchain Breakfast', '2020-10-22 00:31:24', '2020-10-22 00:31:24'),
(2, 'Gokhshtein Unfiltered', '2020-10-22 06:51:00', '2020-10-22 06:51:00');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-10-21 23:49:20', '2020-10-21 23:49:20'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-10-21 23:49:20', '2020-10-21 23:49:20'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-10-21 23:49:20', '2020-10-21 23:49:20'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-10-21 23:49:20', '2020-10-21 23:49:20');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-10-21 23:49:11', '2020-10-21 23:49:11'),
(2, 'user', 'Normal User', '2020-10-21 23:49:11', '2020-10-21 23:49:11'),
(3, 'developer', 'Developer', '2020-10-21 23:54:02', '2020-10-21 23:54:02');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Gokhshtein CMS', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Admin Panel for Gokhshtein', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\October2020\\PJ8VsidiDBermsvdEloX.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-10-21 23:49:21', '2020-10-21 23:49:21'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-10-21 23:49:22', '2020-10-21 23:49:22'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-10-21 23:49:23', '2020-10-21 23:49:23'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-10-21 23:49:23', '2020-10-21 23:49:23'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-10-21 23:49:23', '2020-10-21 23:49:23'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-10-21 23:49:23', '2020-10-21 23:49:23'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-10-21 23:49:23', '2020-10-21 23:49:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$pMuehhPG.YvcOvFkG8p2gusBh7HlEk7rOO7cXnl/5So5qDAk4Ndt6', 'JOqe6s6TIysG8Rm3oyFDCn4eUMz67RBUFy6WmNRnZffIkHyRe6eSLRAGF0p8', NULL, '2020-10-21 23:49:19', '2020-10-21 23:49:19'),
(2, 2, 'Suraj', 'suraj@luxeveda.com', 'users/default.png', NULL, '$2y$10$RbkRibH1PckeWRJ/7QsrSe.s8KmUgCrU2BERE25DiOTPvrqC9oFha', 'z2gW2mG3AIUNENZuqZE02xEzKDjeTsjl0tEcdGpFGkw4k3qwNvjsqyVLY7ji', '{\"locale\":\"en\"}', '2020-10-21 23:53:24', '2020-10-21 23:53:24'),
(3, 3, 'Punith', 'punith@luxeveda.com', 'users/default.png', NULL, '$2y$10$vCmq5kxGjLMchwNND5lNSuH4a1wm3CWe8hswdO9ggLkbf9HIXMp.u', NULL, '{\"locale\":\"en\"}', '2020-10-21 23:54:50', '2020-10-21 23:54:50');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(2, 2),
(3, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `contact_details`
--
ALTER TABLE `contact_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `magazines`
--
ALTER TABLE `magazines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `magazines_categories`
--
ALTER TABLE `magazines_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `magazine_downloads`
--
ALTER TABLE `magazine_downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `podcasts`
--
ALTER TABLE `podcasts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `podcast_categories`
--
ALTER TABLE `podcast_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact_details`
--
ALTER TABLE `contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `magazines`
--
ALTER TABLE `magazines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `magazines_categories`
--
ALTER TABLE `magazines_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `magazine_downloads`
--
ALTER TABLE `magazine_downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `podcasts`
--
ALTER TABLE `podcasts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `podcast_categories`
--
ALTER TABLE `podcast_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
