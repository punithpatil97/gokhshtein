<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Magazine extends Model
{
    public function magazinesApi(){
        $magazines = DB::table('magazines')
                    ->join('magazines_categories', 'magazines.magazine_category', '=', 'magazines_categories.id')
                    ->select('magazines.id','magazines.magazine_title','magazines.magazine_poster','magazines.magazine_description','magazines.magazine_tag','magazines.created_at','magazines_categories.title','magazines.magazine_pages')
                    ->get();
        return $magazines;
    }
}
