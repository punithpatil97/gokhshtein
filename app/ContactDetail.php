<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class ContactDetail extends Model
{
    protected $fillable = [
        'name' , 'email', 'phone_number' , 'message'
        ];
    public $timestamps = false;
  
}