<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class MagazineDownload extends Model
{
    protected $table = 'magazine_downloads';
    protected $fillable = [
        'name', 'phone_number' , 'email' , 'magazine_id'
        ];
    public $timestamps = false;

    public function magazineId($magazineId){
        $magazineLink = DB::table('magazines')
        ->select('magazines.magazine_link')
        ->where('magazines.id', $magazineId)
        ->get();
        return $magazineLink;
    }


}
