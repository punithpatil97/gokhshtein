<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Podcast extends Model
{
    
    public function podcastsApi(){
    
        $podcasts = DB::table('podcasts')
                    ->join('podcast_categories', 'podcasts.podcast_category', '=', 'podcast_categories.id')
                    ->select('podcasts.id','podcasts.podcast_title','podcasts.podcast_image','podcasts.podcast_date','podcasts.podcast_link','podcast_categories.cat_title','podcasts.created_at')
                    ->get();
        return $podcasts;
    }
}
