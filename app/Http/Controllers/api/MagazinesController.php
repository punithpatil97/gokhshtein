<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\MagazineDownload;
use App\Magazine;




class MagazinesController extends Controller
{
    public function __construct()
    {
        $this->imageUrl = base_path() .'/public/storage/' ;
    }


    public function showAllmagazines(){
        $allMagazine = new Magazine();
        $magazineObject = $allMagazine->magazinesApi();
        $magazineArray = array();

        foreach ($magazineObject as $key => $singleMagazine) {
            $magazineArray[$key]['magazineID'] = $singleMagazine->id;
            $magazineArray[$key]['magazineTitle'] = $singleMagazine->magazine_title;
            $magazineArray[$key]['magazinePoster'] = $this->imageUrl . $singleMagazine->magazine_poster;
            $magazineArray[$key]['magazineCategory'] = $singleMagazine->title;
            $magazineArray[$key]['magazineTag'] = $singleMagazine->magazine_tag;
            $magazineArray[$key]['magazinePages'] = $singleMagazine->magazine_pages;
            $magazineArray[$key]['magazineDescription'] = $singleMagazine->magazine_description;
        }
        return $magazineArray;

    }


    public function storeDownloadedData(Request $request){
        $magazineDownload = MagazineDownload::create($request->all());
                if($magazineDownload){
                    $getDownloadLink = new MagazineDownload();
                    $magazineLink = $getDownloadLink->magazineId($request['magazine_id']);
                    foreach ($magazineLink as $key => $magLink) {
                       $jsonMagazinedata = json_decode($magLink->magazine_link);
                       $jsonMagazinedata[0]->download_link = $this->imageUrl . $jsonMagazinedata[0]->download_link;
                      
                    //    $originalName = $jsonMagazinedata[0]->original_name;
                    //    $downloadLinkF = str_replace('\\', '/', $downloadLink);
                    //    $urlBreak = explode('/',  $downloadLinkF);
                    //     $storedMagazine = rename( $this->imageUrl . $downloadLinkF , $this->imageUrl . 'magazines/'. $urlBreak[1] .'/' .$originalName);
                    //     print_r($storedMagazine);
                    }
                    return $jsonMagazinedata;
                   
                }else{
                    return "Failed to store the data!!";
                }
       
    }
    
}