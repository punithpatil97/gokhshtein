<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Podcast;



class PodcastsController extends Controller
{
    public function __construct()
    {
        $this->imageUrl = base_path() .'/public/storage/' ;
    }

    public function getAllpodcasts(){
        $allPodcast = new Podcast();
        $podcastObject = $allPodcast->podcastsApi();
        $podcastArray = array();
        foreach ($podcastObject as $key => $singlePodcast) {
            $podcastArray[$key]['podcastID'] = $singlePodcast->id;
            $podcastArray[$key]['podcastTitle'] = $singlePodcast->podcast_title;
            $podcastArray[$key]['podcastImage'] = $this->imageUrl . $singlePodcast->podcast_image;
            $podcastArray[$key]['podcastDate'] = $singlePodcast->podcast_date .'T00:00:00Z';
            $podcastArray[$key]['podcastLink'] = $singlePodcast->podcast_link;
            $podcastArray[$key]['podcastCategory'] = $singlePodcast->cat_title;
        }

        return $podcastArray;

    }
}