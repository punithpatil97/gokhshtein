<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/podcasts', 'Api\PodcastsController@getAllpodcasts');

Route::get('/magazines', 'Api\MagazinesController@showAllmagazines');

Route::post('/download_data', 'Api\MagazinesController@storeDownloadedData');


Route::post('/contact_us', 'Api\ContactController@storeContact');